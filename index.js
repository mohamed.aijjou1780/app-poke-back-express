const express = require("express");
const cors = require("cors");

const bodyParser = require("body-parser");
const app = express();

const fs = require("fs");

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
  res.header("Access-Control-Allow-Headers", "*");
  next();
});

app.use(bodyParser.json());
app.use(cors());

app.listen(82, function () {
  console.log("connexion");
});

const getPokemeon = () => {
  const pokemonJson = fs.readFileSync("pokemon.json");
  const pokemons = JSON.parse(pokemonJson);
  return pokemons;
};

const savePokemon = (data) => {
  fs.writeFileSync("pokemon.json", JSON.stringify(data));
};

const saveUser = (data) => {
  fs.writeFileSync("user.json", JSON.stringify(data));
};

const getUsers = () => {
  const contentUser = fs.readFileSync("user.json");
  const users = JSON.parse(contentUser);
  return users;
};

app.get("/pokemons", (req, res) => {
  console.log("appel liste pokemons");
  res.json(getPokemeon());
});

app.post("/save-pokemon", (req, res) => {
  console.log("save pokemon");
  const pokemonAcree = req.body;
  const tmpPokemon = [...getPokemeon(), pokemonAcree];
  savePokemon(tmpPokemon);
  result = "pokemon crée";
  return res.json({ msg: result });
});

app.post("/creation-user", (req, res) => {
  const dataCreation = req.body;

  const user = {
    email: dataCreation.email,
    password: dataCreation.password,
    adresse: dataCreation.adresse,
    adresse2: dataCreation.adresse2,
    city: dataCreation.city,
    state: dataCreation.state,
    zip: dataCreation.zip,
  };

  const tmpUsers = [...getUsers(), user];
  saveUser(tmpUsers)
  return res.status(200).json({ isCreated: true });
});
